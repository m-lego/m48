4x12 in lego
===============

  ![M48 4x12 full](pics/4x12/m48_tty.jpg)

  ![M48 4x12 no keycaps](pics/4x12/m48.jpg)

this is practlically a planck style.

an ortholinear keyboard set in lego 4x12 with optional encoder and leds
this is part of a bigger family of ortholinear keyboards in lego see for reference
https://alin.elena.space/blog/keeblego/

current status and more

https://gitlab.com/m-lego/m65

kicad symbols/footprints are in the m65 repo above


status: tested all ok!

* [x] gerbers designed
* [x] firmware
* [x] breadboard tested
* [x] gerbers printed
* [x] board tested


Features:

* 4x12
* 1 encoder
* led strip (optional)
* 5 pins
* stm32f401 or 411(if you ask) from we act https://github.com/WeActTC/MiniSTM32F4x1
* firmware qmk


the pcb
-------

* front

  ![M48 4x12 pcb](pics/4x12/m48-pcb-back.jpg)


* back

  ![M48 4x12 pcb](pics/4x12/m48-pcb-front.jpg)


* kicad

  ![M48 4x12 pcb](pics/4x12/m48-pcb.png)

parts
-----


* 1xSTM32F401 we act pins
* 48 signal diodes 1N4148 , do 35
* 1 encoder
* 2x220Ω or 350Ω these are for leds so you may have to compute the R to match your colours and desired brightness.
* 1 leds
* 1 40 pin DIL/DIP sockets whatever you prefer
* led strip 3pins, optional
* 5 pin MX switches 48
* lego see main article, and imagination.
* lego 32x16 plates for bottom, and bricks as you please


repo
----

gitlab repo https://gitlab.com/m-lego/m48/


firmware
--------

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make m-lego/m48/rev1:default
   make m-lego/m48/rev1:default:flash
```
